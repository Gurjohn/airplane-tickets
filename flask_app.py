# A very simple Flask Hello World app for you to get started with...
import requests
from flask import Flask, render_template , request, jsonify, url_for, redirect


app = Flask(__name__)


@app.route('/')
def hello_world():
    return redirect(url_for('.flights'))



@app.route('/flights', methods=['GET' , 'POST'])
def flights():
    if request.method == 'POST':
        r = request.form
        date_from = '{}/{}/{}'.format(r.get('dayfrom'), r.get('monthfrom'), r.get('yearfrom'))
        date_to = '{}/{}/{}'.format(r.get('dayto'), r.get('monthto'), r.get('yearto'))
        payload={'fly_from': r.get('from'),'fly_to': r.get('to') ,'date_from': date_from ,'date_to': date_to, 'partner':'picky' }


        data = requests.get('https://api.skypicker.com/flights' , params = payload)
        return jsonify(data.json()['data'])
    else:
        return render_template('index.html')

